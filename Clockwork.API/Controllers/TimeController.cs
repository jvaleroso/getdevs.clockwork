﻿using System;
using Microsoft.AspNetCore.Mvc;
using Clockwork.API.Models;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Clockwork.API.Data;
using System.Linq;

namespace Clockwork.API.Controllers
{
    [Route("api/[controller]")]
    public class TimeController : ControllerBase
    {
        private readonly ClockworkContext _context;

        public TimeController(ClockworkContext context)
        {
            _context = context;
        }

        
        [HttpGet]
        [Route("GetCurrentTimeQueries")]
        public async Task<List<CurrentTimeQuery>> GetCurrentTimeQueries()
        {
            var items = await _context.CurrentTimeQueries.ToListAsync();
            return items.OrderBy(item => item.Time).ToList();
        }

        [HttpGet]
        [Route("GetCurrentTimeQuery")]
        public Task<CurrentTimeQuery> GetCurrentTimeQuery()
        {
            var utcTime = DateTime.UtcNow;
            var serverTime = DateTime.Now;
            var ip = this.HttpContext.Connection.RemoteIpAddress.ToString();

            var returnVal = new CurrentTimeQuery
            {
                UTCTime = utcTime,
                ClientIp = ip,
                Time = serverTime,
                TimeZone= TimeZoneInfo.Local.StandardName
            };

            _context.Add(returnVal);
            var count = _context.SaveChangesAsync();
            Console.WriteLine("{0} records saved to database", count);

            Console.WriteLine();
            foreach (var CurrentTimeQuery in _context.CurrentTimeQueries)
            {
                Console.WriteLine(" - {0}", CurrentTimeQuery.UTCTime);
            }

            return Task.FromResult(returnVal);
        }

        [HttpGet]
        [Route("GetLatestTimeQueries")]
        public async Task<List<CurrentTimeQuery>> GetLatestTimeQueries()
        {
            var utcTime = DateTime.UtcNow;
            var serverTime = DateTime.Now;
            var ip = this.HttpContext.Connection.RemoteIpAddress.ToString();

            var returnVal = new CurrentTimeQuery
            {
                UTCTime = utcTime,
                ClientIp = ip,
                Time = serverTime,
                TimeZone = TimeZoneInfo.Local.StandardName
            };

            _context.Add(returnVal);
            var count = _context.SaveChangesAsync();
            Console.WriteLine("{0} records saved to database", count);

            var items= await _context.CurrentTimeQueries.ToListAsync();
            return items.OrderBy(item => item.Time).ToList();
        }

        [HttpGet]
        [Route("GetCurrentTime")]
        public async Task<CurrentTimeQuery> GetCurrentTime()
        {
            var utcTime = DateTime.UtcNow;
            var serverTime = DateTime.Now;
            var ip = this.HttpContext.Connection.RemoteIpAddress.ToString();

            var currentTime = new CurrentTimeQuery
            {
                UTCTime = utcTime,
                ClientIp = ip,
                Time = serverTime,
                TimeZone = TimeZoneInfo.Local.StandardName
            };

            _context.Add(currentTime);
            var count =await _context.SaveChangesAsync();
            Console.WriteLine("{0} records saved to database", count);

            return currentTime;
        }
    }
}
