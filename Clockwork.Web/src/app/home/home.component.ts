import { Component, OnInit } from '@angular/core';
import { ClockworkService } from '../clockwork.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    public CurrentTimeQuery;
    public CurrentTimeQueries;
    constructor(private clockworkService: ClockworkService) {
    }

    ngOnInit() {
        this.loadCurrentTimeQueries();
    }

    loadCurrentTime() {
        this.loadLatestTimeQueries();
    }

    public loadCurrentTimeQueries() {
        this.clockworkService.getCurrentTimeQueries().subscribe((data) => {
            this.CurrentTimeQueries = data;
        });
    }

    public loadLatestTimeQueries() {
        this.clockworkService.getLatestTimeQueries().subscribe((data) => {
            this.CurrentTimeQueries = data;
        });
    }
}
