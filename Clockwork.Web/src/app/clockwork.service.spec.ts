import { TestBed } from '@angular/core/testing';

import { ClockworkService } from './clockwork.service';

describe('ClockworkService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClockworkService = TestBed.get(ClockworkService);
    expect(service).toBeTruthy();
  });
});
