export class Configuration {
    public server = 'http://localhost:5000/';
    public apiUrl = 'api/time';
    public serverWithApiUrl = this.server + this.apiUrl;
}