import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from "rxjs/operators";
import { Observable } from "rxjs";
import { CurrentTimeQuery } from './current-time-query';
import { Configuration } from './configuration';

@Injectable({
  providedIn: 'root'
})
export class ClockworkService {

    private actionUrl: string;
    constructor(private httpClient: HttpClient, private configuration: Configuration) {
        this.actionUrl = configuration.serverWithApiUrl;
    }

    getCurrentTimeQueries(): Observable<CurrentTimeQuery> {
        return this.httpClient.get(this.actionUrl + '/getCurrentTimeQueries').
            pipe(
            map((item: any) => item.map(p => <CurrentTimeQuery>
                    {
                        currentTimeQueryId: p.currentTimeQueryId,
                        time: p.time,
                        clientIp: p.clientIp,
                        utcTime: p.utcTime,
                        timeZone: p.timeZone
                    })));
    };

    getLatestTimeQueries(): Observable<CurrentTimeQuery> {
        return this.httpClient.get(this.actionUrl + '/getLatestTimeQueries').
            pipe(
                map((item: any) => item.map(p => <CurrentTimeQuery>
                    {
                        currentTimeQueryId: p.currentTimeQueryId,
                        time: p.time,
                        clientIp: p.clientIp,
                        utcTime: p.utcTime,
                        timeZone: p.timeZone
                    })));
    }

}
