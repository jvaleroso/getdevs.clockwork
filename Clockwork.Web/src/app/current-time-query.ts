export class CurrentTimeQuery {
    currentTimeQueryId: number;
    time: string;
    clientIp: string;
    utcTime: string;
    timeZone: string;
}
