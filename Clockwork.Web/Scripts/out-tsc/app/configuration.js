"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Configuration = /** @class */ (function () {
    function Configuration() {
        this.server = 'http://localhost:5000/';
        this.apiUrl = 'api/time';
        this.serverWithApiUrl = this.server + this.apiUrl;
    }
    return Configuration;
}());
exports.Configuration = Configuration;
//# sourceMappingURL=configuration.js.map